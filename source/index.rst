Welcome to Inkscape's Extension Docs
====================================

Welcome. We hope you enjoy your stay here.
If you're just starting out, check out our getting started guide
:doc:`here </01_getting-started>`.

.. toctree::
   :maxdepth: 2
   :caption: Guide

   01_getting-started
   02_hello-world
   03_support

.. toctree::
   :maxdepth: 4
   :caption: Inkex API

   inkex-core
   inkex-modules
   inkex-tester



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
